﻿namespace TwitchAPIViewer
{
    partial class frmTwitchAlertsSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTwitchAlertsSettings));
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTwitchAlerts = new System.Windows.Forms.TextBox();
            this.txtRecentFollower = new System.Windows.Forms.TextBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnFindTwitchAlerts = new System.Windows.Forms.Button();
            this.btnFindRecentFollower = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.epTwitchAlerts = new System.Windows.Forms.ErrorProvider(this.components);
            this.epRecentFollower = new System.Windows.Forms.ErrorProvider(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.epTwitchAlerts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.epRecentFollower)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(12, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 23);
            this.label2.TabIndex = 0;
            this.label2.Text = "TwitchAlerts";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.Location = new System.Drawing.Point(12, 120);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 23);
            this.label3.TabIndex = 0;
            this.label3.Text = "Recent Follower";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTwitchAlerts
            // 
            this.txtTwitchAlerts.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTwitchAlerts.Location = new System.Drawing.Point(119, 12);
            this.txtTwitchAlerts.Multiline = true;
            this.txtTwitchAlerts.Name = "txtTwitchAlerts";
            this.txtTwitchAlerts.Size = new System.Drawing.Size(222, 106);
            this.txtTwitchAlerts.TabIndex = 2;
            this.txtTwitchAlerts.Text = resources.GetString("txtTwitchAlerts.Text");
            this.txtTwitchAlerts.TextChanged += new System.EventHandler(this.txtTwitchAlerts_TextChanged);
            // 
            // txtRecentFollower
            // 
            this.txtRecentFollower.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRecentFollower.Location = new System.Drawing.Point(119, 124);
            this.txtRecentFollower.Name = "txtRecentFollower";
            this.txtRecentFollower.Size = new System.Drawing.Size(222, 20);
            this.txtRecentFollower.TabIndex = 3;
            this.txtRecentFollower.Text = "G:\\TwitchAlerts";
            this.txtRecentFollower.TextChanged += new System.EventHandler(this.txtRecentFollower_TextChanged);
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClear.Location = new System.Drawing.Point(13, 152);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 4;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // btnFindTwitchAlerts
            // 
            this.btnFindTwitchAlerts.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFindTwitchAlerts.Location = new System.Drawing.Point(347, 10);
            this.btnFindTwitchAlerts.Name = "btnFindTwitchAlerts";
            this.btnFindTwitchAlerts.Size = new System.Drawing.Size(75, 23);
            this.btnFindTwitchAlerts.TabIndex = 5;
            this.btnFindTwitchAlerts.Text = "Find";
            this.btnFindTwitchAlerts.UseVisualStyleBackColor = true;
            this.btnFindTwitchAlerts.Click += new System.EventHandler(this.btnFindTwitchAlerts_Click);
            // 
            // btnFindRecentFollower
            // 
            this.btnFindRecentFollower.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFindRecentFollower.Location = new System.Drawing.Point(347, 122);
            this.btnFindRecentFollower.Name = "btnFindRecentFollower";
            this.btnFindRecentFollower.Size = new System.Drawing.Size(75, 23);
            this.btnFindRecentFollower.TabIndex = 6;
            this.btnFindRecentFollower.Text = "Find";
            this.btnFindRecentFollower.UseVisualStyleBackColor = true;
            this.btnFindRecentFollower.Click += new System.EventHandler(this.btnFindRecentFollower_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(347, 151);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.InitialDirectory = "C:\\Users\\caLLow\\AppData\\Local\\Apps\\2.0";
            // 
            // epTwitchAlerts
            // 
            this.epTwitchAlerts.ContainerControl = this;
            // 
            // epRecentFollower
            // 
            this.epRecentFollower.ContainerControl = this;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // frmTwitchAlertsSettings
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 186);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnFindRecentFollower);
            this.Controls.Add(this.btnFindTwitchAlerts);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.txtRecentFollower);
            this.Controls.Add(this.txtTwitchAlerts);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(442, 208);
            this.Name = "frmTwitchAlertsSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "TwitchAlerts Settings";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSettings_FormClosing);
            this.Load += new System.EventHandler(this.frmSettings_Load);
            ((System.ComponentModel.ISupportInitialize)(this.epTwitchAlerts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.epRecentFollower)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtTwitchAlerts;
        private System.Windows.Forms.TextBox txtRecentFollower;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnFindRecentFollower;
        private System.Windows.Forms.Button btnFindTwitchAlerts;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ErrorProvider epRecentFollower;
        private System.Windows.Forms.ErrorProvider epTwitchAlerts;
        private System.Windows.Forms.Timer timer1;
    }
}