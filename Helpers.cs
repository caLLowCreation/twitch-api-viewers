﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using System.Windows.Forms;
using System.Diagnostics;

namespace TwitchAPIViewer
{
    public class Helpers
    {
        internal const string TWITCH_ALERTS_PROCESS_NAME = "TwitchAlerts";

        internal static bool IsValidChannel(string channelUserName)
        {
            WebClient webClient = new WebClient();
            var url = string.Format("https://api.twitch.tv/kraken/channels/{0}", channelUserName);
            string json = string.Empty;
            try
            {
                json = webClient.DownloadString(url);
            }
            catch (Exception e)
            {
                throw e;
            }
            return JsonConvert.DeserializeObject<Channel>(json) != null;
        }

        internal static void LoadToBrowser(WebBrowser webBrowser, string urlFormat, string channelName)
        {
            try
            {
                if (Helpers.IsValidChannel(channelName))
                {
                    webBrowser.Navigate(string.Format(urlFormat, channelName));
                }
            }
            catch (Exception)
            {
                //Navigate to OAuth
                //wbStream.Navigate("http://www.twitch.tv/");
            }
        }

        internal static void TryAddChannelUser(string userName)
        {
            if (!string.IsNullOrWhiteSpace(userName))
            {
                userName = userName.ToLower();
                if (!Properties.Settings.Default.ChannelUsers.Contains(userName))
                {
                    Properties.Settings.Default.ChannelUsers.Add(userName);
                }
                var channelUsers = new List<string>();
                foreach (var channelUser in Properties.Settings.Default.ChannelUsers)
                {
                    channelUsers.Add(channelUser);
                }
                channelUsers = channelUsers.OrderByDescending(x => x == userName).ToList();
                Properties.Settings.Default.ChannelUsers.Clear();
                Properties.Settings.Default.ChannelUsers.AddRange(channelUsers.ToArray());
            }
        }

        internal static void StartUpOnUserChanged(object sender, params Action[] loadProcedures)
        {
            ToolStripComboBox tscmb = sender as ToolStripComboBox;
            ContextMenuStrip parent = tscmb.Owner as ContextMenuStrip;
            if (parent != null)
            {
                parent.Close();
            }
            Helpers.StartUpFromUserNameChanged(tscmb.Text, loadProcedures);
        }

        internal static void StartUpFromUserNameChanged(string userName, params Action[] loadProcedures)
        {
            Helpers.TryAddChannelUser(userName);
            foreach (var loacProc in loadProcedures)
            {
                loacProc.Invoke();
            }
        }
    }
}
