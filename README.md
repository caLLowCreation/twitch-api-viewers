# README #
MIT License (MIT)

The solution requires at least .NET 4.5 and Visual Studio 2013

Twitch API Viewer is a overlay window that streamers who have one monitor to see and interact with their viewers without the need for an open browser. On my machine the difference in memory saved in in the order of 2+MB. In this video I show advances in the original Twitch API Viewer, on YouTube, in which the stream and chat popout are utilized.

### What is this repository for? ###

* Host a complete solution for quick start and build
* Version Assembly 1.0.0 (Publish 1.0.0.25 @ time of this edit)

### How do I get set up? ###

* Summary of set up - download and open in Visual Studio
* Configuration - Not Required
* Dependencies - Included
* Database configuration - Not Required uses Settings
* How to run tests - Not Used
* Deployment instructions - MIT License (MIT)

### Contribution guidelines ###

* Writing tests - Not Required BUT NEEDED!
* Code review - Not Required but wanted
* Other guidelines - Any input will be considered for implementation

### Who do I talk to? ###

* Repo owner or admin - caLLowCreation
* Other community or team contact