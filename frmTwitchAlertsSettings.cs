﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TwitchAPIViewer
{
    public partial class frmTwitchAlertsSettings : Form
    {

        public class ValidatedInput
        {
            public readonly Control control;
            public readonly ErrorProvider errorProvider;
            public bool IsValid;
            public string Text;
            public string LastText = string.Empty;
            public string ErrorMessage;
            public string ExceptionMessage;
            public readonly Func<string, bool>[] validators;

            public ValidatedInput(Control control, ErrorProvider errorProvider, string errorMessage, params Func<string, bool>[] validators)
            {
                this.control = control;
                this.errorProvider = errorProvider;
                this.Text = control.Text;
                this.ErrorMessage = errorMessage;
                this.validators = validators;
            }
        }

        internal enum InputId
        {
            ChannelName,
            TwitchAlerts,
            RecentFollower
        }

        bool m_Running;

        Dictionary<InputId, ValidatedInput> m_Validators;

        //internal bool IsReadyToUse { get { return m_Validators.Values.ToList().TrueForAll(x => x.IsValid); } }
        Thread m_ValidatorThread;

        public frmTwitchAlertsSettings()
        {
            InitializeComponent();
            m_Validators = new Dictionary<InputId, ValidatedInput>();
            m_Validators.Add(InputId.TwitchAlerts, new ValidatedInput(txtTwitchAlerts, epTwitchAlerts, "TwitchAlerts executible not found at selected location.", File.Exists));
            m_Validators.Add(InputId.RecentFollower, new ValidatedInput(txtRecentFollower, epRecentFollower, "Can not find recent followers file at this location.", File.Exists));

            txtTwitchAlerts.Text = Properties.Settings.Default.TwitchAlerts;
            txtRecentFollower.Text = Properties.Settings.Default.RecentFollower;

            m_ValidatorThread = new Thread(new ThreadStart(Validator));
            m_ValidatorThread.Start();
        }

        void frmSettings_Load(object sender, EventArgs e)
        {
            foreach (var control in this.Controls)
            {
                var txt = control as TextBox;
                if (txt != null)
                {
                    if (string.IsNullOrWhiteSpace(txt.Text))
                    {
                        txt.Focus();
                        break;
                    }
                }
            }
        }

        internal bool IsValid(InputId inputId)
        {
            return m_Validators[inputId].IsValid;
        }

        internal bool TestValidity(InputId inputId, params Func<string, bool>[] validators)
        {
            return !string.IsNullOrWhiteSpace(m_Validators[inputId].Text) && validators.ToList().TrueForAll(s => s.Invoke(m_Validators[inputId].Text));
        }

        void Validator()
        {
            m_Running = true;

            while (m_Running)
            {
                foreach (var validator in m_Validators)
                {
                    if (validator.Value.Text != validator.Value.LastText)
                    {
                        validator.Value.ExceptionMessage = string.Empty;
                        try
                        {
                            validator.Value.IsValid = TestValidity(validator.Key, validator.Value.validators);
                            if (validator.Value.IsValid)
                            {
                                Properties.Settings.Default.RecentFollower = validator.Value.Text;
                            }
                        }
                        catch (Exception e)
                        {
                            validator.Value.ExceptionMessage = e.Message;
                        }

                        validator.Value.LastText = validator.Value.Text;
                    }
                }

                Thread.Sleep(10);
            }
        }

        internal void StopValidating()
        {
            m_Running = false;
            if (m_ValidatorThread != null)
            {
                try
                {
                    m_ValidatorThread.Abort();
                }
                catch (ThreadAbortException) { }
            }
        }

        void frmSettings_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                Properties.Settings.Default.TwitchAlerts = m_Validators[InputId.TwitchAlerts].Text;
                Properties.Settings.Default.RecentFollower = m_Validators[InputId.RecentFollower].Text;
                Properties.Settings.Default.Save();
            }
        }

        void btnSave_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        void btnFindRecentFollower_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
            {
                m_Validators[InputId.RecentFollower].Text = openFileDialog1.FileName;
                txtRecentFollower.Text = m_Validators[InputId.RecentFollower].Text;
            }
        }

        void btnFindTwitchAlerts_Click(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = @"C:\Users\caLLow\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\TwitchAlerts\";
            if (openFileDialog1.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
            {
                m_Validators[InputId.TwitchAlerts].Text = openFileDialog1.FileName;
                txtTwitchAlerts.Text = m_Validators[InputId.TwitchAlerts].Text;
            }
        }

        void txtTwitchAlerts_TextChanged(object sender, EventArgs e)
        {
            m_Validators[InputId.TwitchAlerts].Text = txtTwitchAlerts.Text;
        }

        void txtRecentFollower_TextChanged(object sender, EventArgs e)
        {
            m_Validators[InputId.RecentFollower].Text = txtRecentFollower.Text;
        }

        void timer1_Tick(object sender, EventArgs e)
        {
            foreach (var validator in m_Validators.Values)
            {
                if (validator.ExceptionMessage != string.Empty)
                {
                    if (!validator.errorProvider.GetError(validator.control).Equals(validator.ExceptionMessage))
                    {
                        validator.errorProvider.SetError(validator.control, validator.ExceptionMessage);
                    }
                }
                else
                {
                    if (validator.IsValid)
                    {
                        if (validator.errorProvider.GetError(validator.control) != string.Empty)
                        {
                            validator.errorProvider.SetError(validator.control, string.Empty);
                        }
                    }
                    else
                    {
                        if (!validator.errorProvider.GetError(validator.control).Equals(validator.ErrorMessage))
                        {
                            validator.errorProvider.SetError(validator.control, validator.ErrorMessage);
                        }
                    }
                }
            }
        }
    }
}
