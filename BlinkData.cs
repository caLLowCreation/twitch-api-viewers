﻿using System.Drawing;
using System.Windows.Forms;

namespace TwitchAPIViewer
{
    public class BlinkData
    {
        public int count;
        public bool loss;
        public Color foreColor;
        public Color backColor;

        public BlinkData(Control ctrlView)
        {
            this.foreColor = ctrlView.ForeColor;
            this.backColor = ctrlView.BackColor;
        }
    }
}
