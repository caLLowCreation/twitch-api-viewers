﻿namespace TwitchAPIViewer
{
    partial class frmTwitchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTwitchForm));
            this.lblViewers = new System.Windows.Forms.Label();
            this.ctxChangeUser = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.chatUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tscmbChangeUsers = new System.Windows.Forms.ToolStripComboBox();
            this.lblFollowers = new System.Windows.Forms.Label();
            this.lblViews = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pnlQuickView = new System.Windows.Forms.Panel();
            this.btnToggleSize = new System.Windows.Forms.Button();
            this.lblUpTime = new System.Windows.Forms.Label();
            this.ctxmUpTime = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.upTimeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.restartToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tbOpacity = new System.Windows.Forms.TrackBar();
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.lblMostVisits = new System.Windows.Forms.Label();
            this.lblMostViewers = new System.Windows.Forms.Label();
            this.lblRecentFollower = new System.Windows.Forms.Label();
            this.ctxTwitchAlertsUser = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.picRecentFollower = new System.Windows.Forms.PictureBox();
            this.picOnline = new System.Windows.Forms.PictureBox();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.wbStream = new System.Windows.Forms.WebBrowser();
            this.pnlUpTime = new System.Windows.Forms.Panel();
            this.chkOpenOnChat = new System.Windows.Forms.CheckBox();
            this.wbChat = new System.Windows.Forms.WebBrowser();
            this.chkStream = new System.Windows.Forms.CheckBox();
            this.ctxChangeUser.SuspendLayout();
            this.pnlQuickView.SuspendLayout();
            this.ctxmUpTime.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbOpacity)).BeginInit();
            this.pnlBottom.SuspendLayout();
            this.ctxTwitchAlertsUser.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picRecentFollower)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOnline)).BeginInit();
            this.pnlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.pnlUpTime.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblViewers
            // 
            this.lblViewers.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblViewers.ContextMenuStrip = this.ctxChangeUser;
            this.lblViewers.Location = new System.Drawing.Point(-1, 1);
            this.lblViewers.Name = "lblViewers";
            this.lblViewers.Size = new System.Drawing.Size(91, 13);
            this.lblViewers.TabIndex = 0;
            this.lblViewers.Tag = "Twitch API Users";
            this.lblViewers.Text = "Viewers: 000/000";
            this.lblViewers.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // ctxChangeUser
            // 
            this.ctxChangeUser.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.chatUserToolStripMenuItem,
            this.tscmbChangeUsers});
            this.ctxChangeUser.Name = "ctxChatUser";
            this.ctxChangeUser.ShowImageMargin = false;
            this.ctxChangeUser.Size = new System.Drawing.Size(157, 51);
            this.ctxChangeUser.Opening += new System.ComponentModel.CancelEventHandler(this.ctxChangeUser_Opening);
            // 
            // chatUserToolStripMenuItem
            // 
            this.chatUserToolStripMenuItem.Enabled = false;
            this.chatUserToolStripMenuItem.Name = "chatUserToolStripMenuItem";
            this.chatUserToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.chatUserToolStripMenuItem.Text = "Chat Users";
            this.chatUserToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tscmbChangeUsers
            // 
            this.tscmbChangeUsers.DropDownWidth = 131;
            this.tscmbChangeUsers.Name = "tscmbChangeUsers";
            this.tscmbChangeUsers.Size = new System.Drawing.Size(121, 21);
            // 
            // lblFollowers
            // 
            this.lblFollowers.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFollowers.ContextMenuStrip = this.ctxChangeUser;
            this.lblFollowers.Location = new System.Drawing.Point(90, 1);
            this.lblFollowers.Name = "lblFollowers";
            this.lblFollowers.Size = new System.Drawing.Size(75, 13);
            this.lblFollowers.TabIndex = 1;
            this.lblFollowers.Tag = "Twitch API Users";
            this.lblFollowers.Text = "Followers: 000";
            // 
            // lblViews
            // 
            this.lblViews.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblViews.ContextMenuStrip = this.ctxChangeUser;
            this.lblViews.Location = new System.Drawing.Point(165, 1);
            this.lblViews.Name = "lblViews";
            this.lblViews.Size = new System.Drawing.Size(59, 13);
            this.lblViews.TabIndex = 2;
            this.lblViews.Tag = "Twitch API Users";
            this.lblViews.Text = "Views: 000";
            // 
            // timer1
            // 
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // pnlQuickView
            // 
            this.pnlQuickView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlQuickView.Controls.Add(this.btnToggleSize);
            this.pnlQuickView.Controls.Add(this.lblFollowers);
            this.pnlQuickView.Controls.Add(this.lblViews);
            this.pnlQuickView.Controls.Add(this.lblViewers);
            this.pnlQuickView.Location = new System.Drawing.Point(2, 2);
            this.pnlQuickView.Name = "pnlQuickView";
            this.pnlQuickView.Size = new System.Drawing.Size(243, 14);
            this.pnlQuickView.TabIndex = 3;
            // 
            // btnToggleSize
            // 
            this.btnToggleSize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnToggleSize.BackColor = System.Drawing.Color.DimGray;
            this.btnToggleSize.Location = new System.Drawing.Point(228, 1);
            this.btnToggleSize.Name = "btnToggleSize";
            this.btnToggleSize.Size = new System.Drawing.Size(12, 12);
            this.btnToggleSize.TabIndex = 3;
            this.btnToggleSize.Text = "-";
            this.btnToggleSize.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnToggleSize.UseVisualStyleBackColor = false;
            this.btnToggleSize.Click += new System.EventHandler(this.btnToggleSize_Click);
            // 
            // lblUpTime
            // 
            this.lblUpTime.ContextMenuStrip = this.ctxmUpTime;
            this.lblUpTime.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblUpTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUpTime.Location = new System.Drawing.Point(0, 0);
            this.lblUpTime.Name = "lblUpTime";
            this.lblUpTime.Size = new System.Drawing.Size(90, 17);
            this.lblUpTime.TabIndex = 1;
            this.lblUpTime.Text = "00:00:000000";
            this.lblUpTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblUpTime.DoubleClick += new System.EventHandler(this.lblUpTime_DoubleClick);
            // 
            // ctxmUpTime
            // 
            this.ctxmUpTime.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.upTimeToolStripMenuItem,
            this.resetToolStripMenuItem,
            this.restartToolStripMenuItem});
            this.ctxmUpTime.Name = "ctxmUpTime";
            this.ctxmUpTime.ShowImageMargin = false;
            this.ctxmUpTime.Size = new System.Drawing.Size(88, 70);
            // 
            // upTimeToolStripMenuItem
            // 
            this.upTimeToolStripMenuItem.Enabled = false;
            this.upTimeToolStripMenuItem.Name = "upTimeToolStripMenuItem";
            this.upTimeToolStripMenuItem.Size = new System.Drawing.Size(87, 22);
            this.upTimeToolStripMenuItem.Text = "Up Time";
            // 
            // resetToolStripMenuItem
            // 
            this.resetToolStripMenuItem.Name = "resetToolStripMenuItem";
            this.resetToolStripMenuItem.Size = new System.Drawing.Size(87, 22);
            this.resetToolStripMenuItem.Text = "Reset";
            this.resetToolStripMenuItem.Click += new System.EventHandler(this.resetToolStripMenuItem_Click);
            // 
            // restartToolStripMenuItem
            // 
            this.restartToolStripMenuItem.Name = "restartToolStripMenuItem";
            this.restartToolStripMenuItem.Size = new System.Drawing.Size(87, 22);
            this.restartToolStripMenuItem.Text = "Restart";
            this.restartToolStripMenuItem.Click += new System.EventHandler(this.restartToolStripMenuItem_Click);
            // 
            // tbOpacity
            // 
            this.tbOpacity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbOpacity.AutoSize = false;
            this.tbOpacity.ContextMenuStrip = this.ctxChangeUser;
            this.tbOpacity.Location = new System.Drawing.Point(2, 18);
            this.tbOpacity.Maximum = 100;
            this.tbOpacity.Minimum = 45;
            this.tbOpacity.Name = "tbOpacity";
            this.tbOpacity.Size = new System.Drawing.Size(243, 14);
            this.tbOpacity.TabIndex = 5;
            this.tbOpacity.Tag = "Stream Users";
            this.tbOpacity.TickStyle = System.Windows.Forms.TickStyle.None;
            this.tbOpacity.Value = 80;
            this.tbOpacity.ValueChanged += new System.EventHandler(this.tbOpacity_ValueChanged);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlBottom.Controls.Add(this.lblMostVisits);
            this.pnlBottom.Controls.Add(this.lblMostViewers);
            this.pnlBottom.Controls.Add(this.lblRecentFollower);
            this.pnlBottom.Controls.Add(this.picRecentFollower);
            this.pnlBottom.Location = new System.Drawing.Point(0, 540);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(247, 27);
            this.pnlBottom.TabIndex = 6;
            // 
            // lblMostVisits
            // 
            this.lblMostVisits.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMostVisits.ContextMenuStrip = this.ctxChangeUser;
            this.lblMostVisits.Font = new System.Drawing.Font("Arial", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMostVisits.Location = new System.Drawing.Point(205, 1);
            this.lblMostVisits.Name = "lblMostVisits";
            this.lblMostVisits.Size = new System.Drawing.Size(37, 26);
            this.lblMostVisits.TabIndex = 2;
            this.lblMostVisits.Tag = "Twitch API Users";
            this.lblMostVisits.Text = "Visits 0000";
            this.lblMostVisits.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMostViewers
            // 
            this.lblMostViewers.ContextMenuStrip = this.ctxChangeUser;
            this.lblMostViewers.Font = new System.Drawing.Font("Arial", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMostViewers.Location = new System.Drawing.Point(0, 0);
            this.lblMostViewers.Name = "lblMostViewers";
            this.lblMostViewers.Size = new System.Drawing.Size(45, 27);
            this.lblMostViewers.TabIndex = 1;
            this.lblMostViewers.Tag = "Twitch API Users";
            this.lblMostViewers.Text = "Viewers 0000";
            this.lblMostViewers.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRecentFollower
            // 
            this.lblRecentFollower.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRecentFollower.ContextMenuStrip = this.ctxTwitchAlertsUser;
            this.lblRecentFollower.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRecentFollower.Location = new System.Drawing.Point(46, -1);
            this.lblRecentFollower.Name = "lblRecentFollower";
            this.lblRecentFollower.Size = new System.Drawing.Size(165, 26);
            this.lblRecentFollower.TabIndex = 0;
            this.lblRecentFollower.Text = "Recent Follower";
            this.lblRecentFollower.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ctxTwitchAlertsUser
            // 
            this.ctxTwitchAlertsUser.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2});
            this.ctxTwitchAlertsUser.Name = "ctxStreamUser";
            this.ctxTwitchAlertsUser.ShowImageMargin = false;
            this.ctxTwitchAlertsUser.Size = new System.Drawing.Size(151, 26);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(150, 22);
            this.toolStripMenuItem2.Text = "TwitchAlerts Settings";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // picRecentFollower
            // 
            this.picRecentFollower.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.picRecentFollower.BackColor = System.Drawing.Color.Red;
            this.picRecentFollower.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picRecentFollower.Location = new System.Drawing.Point(45, 24);
            this.picRecentFollower.Name = "picRecentFollower";
            this.picRecentFollower.Size = new System.Drawing.Size(160, 4);
            this.picRecentFollower.TabIndex = 3;
            this.picRecentFollower.TabStop = false;
            // 
            // picOnline
            // 
            this.picOnline.BackColor = System.Drawing.Color.Red;
            this.picOnline.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picOnline.Location = new System.Drawing.Point(0, 0);
            this.picOnline.Name = "picOnline";
            this.picOnline.Size = new System.Drawing.Size(247, 567);
            this.picOnline.TabIndex = 3;
            this.picOnline.TabStop = false;
            // 
            // pnlMain
            // 
            this.pnlMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlMain.Controls.Add(this.splitContainer1);
            this.pnlMain.Location = new System.Drawing.Point(2, 32);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(244, 506);
            this.pnlMain.TabIndex = 8;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.wbStream);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.pnlUpTime);
            this.splitContainer1.Panel2.Controls.Add(this.wbChat);
            this.splitContainer1.Size = new System.Drawing.Size(244, 506);
            this.splitContainer1.SplitterDistance = 143;
            this.splitContainer1.TabIndex = 8;
            // 
            // wbStream
            // 
            this.wbStream.ContextMenuStrip = this.ctxChangeUser;
            this.wbStream.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wbStream.IsWebBrowserContextMenuEnabled = false;
            this.wbStream.Location = new System.Drawing.Point(0, 0);
            this.wbStream.MinimumSize = new System.Drawing.Size(20, 20);
            this.wbStream.Name = "wbStream";
            this.wbStream.ScriptErrorsSuppressed = true;
            this.wbStream.ScrollBarsEnabled = false;
            this.wbStream.Size = new System.Drawing.Size(244, 143);
            this.wbStream.TabIndex = 0;
            this.wbStream.Tag = "Stream Users";
            this.wbStream.Url = new System.Uri("", System.UriKind.Relative);
            // 
            // pnlUpTime
            // 
            this.pnlUpTime.Controls.Add(this.chkStream);
            this.pnlUpTime.Controls.Add(this.chkOpenOnChat);
            this.pnlUpTime.Controls.Add(this.lblUpTime);
            this.pnlUpTime.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlUpTime.Location = new System.Drawing.Point(0, 342);
            this.pnlUpTime.Name = "pnlUpTime";
            this.pnlUpTime.Size = new System.Drawing.Size(244, 17);
            this.pnlUpTime.TabIndex = 2;
            // 
            // chkOpenOnChat
            // 
            this.chkOpenOnChat.Dock = System.Windows.Forms.DockStyle.Right;
            this.chkOpenOnChat.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkOpenOnChat.Location = new System.Drawing.Point(171, 0);
            this.chkOpenOnChat.Name = "chkOpenOnChat";
            this.chkOpenOnChat.Size = new System.Drawing.Size(73, 17);
            this.chkOpenOnChat.TabIndex = 2;
            this.chkOpenOnChat.Text = "Chat Alert";
            this.chkOpenOnChat.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkOpenOnChat.UseVisualStyleBackColor = true;
            this.chkOpenOnChat.CheckedChanged += new System.EventHandler(this.chkOpenOnChat_CheckedChanged);
            // 
            // wbChat
            // 
            this.wbChat.ContextMenuStrip = this.ctxChangeUser;
            this.wbChat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wbChat.IsWebBrowserContextMenuEnabled = false;
            this.wbChat.Location = new System.Drawing.Point(0, 0);
            this.wbChat.Name = "wbChat";
            this.wbChat.ScriptErrorsSuppressed = true;
            this.wbChat.Size = new System.Drawing.Size(244, 359);
            this.wbChat.TabIndex = 0;
            this.wbChat.Tag = "Chat Users";
            this.wbChat.Url = new System.Uri("", System.UriKind.Relative);
            // 
            // chkStream
            // 
            this.chkStream.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkStream.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkStream.Location = new System.Drawing.Point(80, 0);
            this.chkStream.Name = "chkStream";
            this.chkStream.Size = new System.Drawing.Size(90, 17);
            this.chkStream.TabIndex = 3;
            this.chkStream.Text = "ShowStream";
            this.chkStream.UseVisualStyleBackColor = true;
            this.chkStream.CheckedChanged += new System.EventHandler(this.chkStream_CheckedChanged);
            // 
            // frmTwitchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(247, 567);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.pnlBottom);
            this.Controls.Add(this.tbOpacity);
            this.Controls.Add(this.pnlQuickView);
            this.Controls.Add(this.picOnline);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmTwitchForm";
            this.Opacity = 0.8D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "TwitchViewer";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TwitchForm_FormClosing);
            this.Load += new System.EventHandler(this.TwitchForm_Load);
            this.ctxChangeUser.ResumeLayout(false);
            this.pnlQuickView.ResumeLayout(false);
            this.ctxmUpTime.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tbOpacity)).EndInit();
            this.pnlBottom.ResumeLayout(false);
            this.ctxTwitchAlertsUser.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picRecentFollower)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOnline)).EndInit();
            this.pnlMain.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.pnlUpTime.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblViewers;
        private System.Windows.Forms.Label lblFollowers;
        private System.Windows.Forms.Label lblViews;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel pnlQuickView;
        private System.Windows.Forms.TrackBar tbOpacity;
        private System.Windows.Forms.Panel pnlBottom;
        private System.Windows.Forms.Label lblRecentFollower;
        private System.Windows.Forms.PictureBox picOnline;
        private System.Windows.Forms.Button btnToggleSize;
        private System.Windows.Forms.Label lblMostVisits;
        private System.Windows.Forms.Label lblMostViewers;
        private System.Windows.Forms.PictureBox picRecentFollower;
        private System.Windows.Forms.Label lblUpTime;
        private System.Windows.Forms.ContextMenuStrip ctxmUpTime;
        private System.Windows.Forms.ToolStripMenuItem resetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem restartToolStripMenuItem;
        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.WebBrowser wbStream;
        private System.Windows.Forms.WebBrowser wbChat;
        private System.Windows.Forms.ContextMenuStrip ctxChangeUser;
        private System.Windows.Forms.ToolStripComboBox tscmbChangeUsers;
        private System.Windows.Forms.ContextMenuStrip ctxTwitchAlertsUser;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.Panel pnlUpTime;
        private System.Windows.Forms.CheckBox chkOpenOnChat;
        private System.Windows.Forms.ToolStripMenuItem upTimeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chatUserToolStripMenuItem;
        private System.Windows.Forms.CheckBox chkStream;
    }
}

