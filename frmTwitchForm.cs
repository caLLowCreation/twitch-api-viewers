﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net;
using System.Threading;
using System.Windows.Forms;
using System.Diagnostics;
using Microsoft.VisualBasic;
using System.Reflection;

namespace TwitchAPIViewer
{
    public partial class frmTwitchForm : Form
    {

        internal const string CHANNEL_USERS_FILENAME = "ChannelUsers.txt";
        
        const string CONTEXT_MENU_TITLE_BAD_TAG = "BAD TAG";
        const string CONTEXT_MENU_TITLE_TWITCH_API_USERS = "Twitch API Users";
        const string CONTEXT_MENU_TITLE_CHAT_USERS = "Chat Users";
        const string CONTEXT_MENU_TITLE_STREAM_USERS = "Stream Users";

        const int BLINK_INC_TIME = 5;
        
        delegate void ShowTwitchTV(TwitchTV twitchTV);
        event ShowTwitchTV m_OnShowTwitchTV;


        long m_LastViewers = 0;
        long m_LastFollowers = 0;
        long m_LastViews = 0;

        int m_Visits = 0;
        TwitchTV m_TwitchTV = null;

        string m_LastChatLine = string.Empty;
        Size m_MaxSize = new Size(255, 591);
        Size m_MinSize = new Size(255, 69);

        bool m_BlinkAll = false;
        Dictionary<Control, BlinkData> m_Blinks = new Dictionary<Control, BlinkData>();

        string m_TwitchAPIUser;

        string m_LastFollower = "";
        Stopwatch m_UpTimer = new Stopwatch();
        frmTwitchAlertsSettings m_FormSettings;

        Process m_TwitchAlertsProcess;
        Thread m_ThreadStartTwitchAlerts;
        Thread m_ThreadLoadKraken;
        bool m_KrakenThreadRunning = false;
        bool m_NeedKrakenUpdate = true;

        Dictionary<string, Action[]> m_UserNameChanged;

        public frmTwitchForm()
        {
            InitializeComponent();

            timer1.Enabled = false;

            tscmbChangeUsers.KeyUp += OnUserNameChanged;

            tscmbChangeUsers.SelectedIndexChanged += OnUserNameChanged;
            tscmbChangeUsers.TextChanged += tscmbChangeUsers_TextChanged;

            m_UserNameChanged = new Dictionary<string, Action[]>();
            m_UserNameChanged.Add(CONTEXT_MENU_TITLE_TWITCH_API_USERS, new Action[] { SetFormTitle, StartTwitchAlertsThread, StartKrakenConnectThread });
            m_UserNameChanged.Add(CONTEXT_MENU_TITLE_CHAT_USERS, new Action[] { LoadWebChat });
            m_UserNameChanged.Add(CONTEXT_MENU_TITLE_STREAM_USERS, new Action[] { LoadWebStream });
           

            m_OnShowTwitchTV += TwitchForm_OnShowTwitchTV;

            m_Blinks.Clear();
            m_Blinks.Add(lblViews, new BlinkData(lblViews));
            m_Blinks.Add(lblFollowers, new BlinkData(lblFollowers));
            m_Blinks.Add(lblViewers, new BlinkData(lblViewers));
            m_Blinks.Add(lblRecentFollower, new BlinkData(lblRecentFollower));
            m_Blinks.Add(wbStream, new BlinkData(wbStream));
            m_Blinks.Add(wbChat, new BlinkData(wbChat));
            m_Blinks.Add(btnToggleSize, new BlinkData(btnToggleSize));
            m_Blinks.Add(picRecentFollower, new BlinkData(picRecentFollower));

            foreach (var blink in m_Blinks)
            {
                blink.Key.Click += (object sender, EventArgs e) => { m_Blinks[sender as Control].count += BLINK_INC_TIME; };
            }

            splitContainer1.SplitterDistance = Properties.Settings.Default.LastSplitDistance;
            tbOpacity.Value = Properties.Settings.Default.LastOpacity;

            this.Location = Properties.Settings.Default.LastLocation;
            this.Size = Properties.Settings.Default.LastSize;

            chkStream.Checked = Properties.Settings.Default.ShowStream;
            chkOpenOnChat.Checked = Properties.Settings.Default.OpenOnChat;
            
            m_FormSettings = new frmTwitchAlertsSettings();
            if (!Properties.Settings.Default.SettingsFirstRun)
            {
                Properties.Settings.Default.SettingsFirstRun = true;
                Properties.Settings.Default.Save();
                OpenSettings();
            }

            timer1.Enabled = true;

            if (Properties.Settings.Default.ChannelUsers.Count > 0)
            {
                m_TwitchAPIUser = Properties.Settings.Default.ChannelUsers[0];
                tscmbChangeUsers.Text = m_TwitchAPIUser;
            }
            ShowStreamCheckedState();
        }

        void TwitchForm_Load(object sender, EventArgs e)
        {
            foreach (var userChanged in m_UserNameChanged)
            {
                foreach (var method in userChanged.Value)
                {
                    method.Invoke();
                }
            }
        }

        void TwitchForm_FormClosing(object sender, FormClosingEventArgs e)
        {            
            StopTwitchAlertsProcess();
            StopKrakenThread();
            m_FormSettings.StopValidating();

            Properties.Settings.Default.LastSplitDistance = splitContainer1.SplitterDistance;
            Properties.Settings.Default.LastOpacity = tbOpacity.Value;

            Properties.Settings.Default.LastLocation = this.Location;
            Properties.Settings.Default.LastSize = this.Size;
            Properties.Settings.Default.Save();
        }

        void ShowStreamCheckedState()
        {
            Properties.Settings.Default.ShowStream = chkStream.Checked;
            if (!chkStream.Checked)
            {
                splitContainer1.Panel1Collapsed = true;
            }
            else
            {
                splitContainer1.Panel1Collapsed = false;
            }
        }

        void StartKrakenConnectThread()
        {
            if (m_ThreadLoadKraken == null)
            {
                m_ThreadLoadKraken = new Thread(new ThreadStart(LoadKraken));
                m_ThreadLoadKraken.Start();
            }
        }

        void StartTwitchAlertsThread()
        {
            if (m_FormSettings.IsValid(frmTwitchAlertsSettings.InputId.TwitchAlerts))
            {
                if (m_ThreadStartTwitchAlerts == null)
                {
                    m_ThreadStartTwitchAlerts = new Thread(new ThreadStart(StartTwitchAlerts));
                    m_ThreadStartTwitchAlerts.Start();
                }
            }
        }

        void LoadWebChat()
        {
            Helpers.LoadToBrowser(wbChat, "http://www.twitch.tv/{0}/chat?popout=", tscmbChangeUsers.Text);
        }

        void LoadWebStream()
        {
            Helpers.LoadToBrowser(wbStream, "http://www.twitch.tv/{0}/popout", chkStream.Checked ? tscmbChangeUsers.Text : "kraken");
        }

        void StopTwitchAlertsProcess()
        {
            try
            {
                if (m_ThreadStartTwitchAlerts != null)
                {
                    m_ThreadStartTwitchAlerts.Abort();
                }
            }
            catch (ThreadAbortException) { }
            finally
            {
                m_ThreadStartTwitchAlerts = null;
            }

            try
            {
                bool killProc = 
                    (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed && 
                    Process.GetProcessesByName(Helpers.TWITCH_ALERTS_PROCESS_NAME).Length == 1) ||
                (!System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed &&
                    Process.GetProcessesByName(Helpers.TWITCH_ALERTS_PROCESS_NAME).Length == 0);
                
                if (killProc && m_TwitchAlertsProcess != null)
                {
                    m_TwitchAlertsProcess.Kill();
                }

            }
            catch (Exception) { }
        }

        void StopKrakenThread()
        {
            m_KrakenThreadRunning = false;
            try
            {
                if (m_ThreadLoadKraken != null)
                {
                    m_ThreadLoadKraken.Abort();
                }
            }
            catch (ThreadAbortException) { }
            finally
            {
                m_ThreadLoadKraken = null;
            }
        }

        void LoadKraken()
        {
            m_KrakenThreadRunning = true;
            WebClient webClient = new WebClient();
            while (m_KrakenThreadRunning)
            {
                try
                {
                    if(m_NeedKrakenUpdate) 
                    {
                        var url = string.Format("https://api.twitch.tv/kraken/streams/{0}", m_TwitchAPIUser);
                        string json = webClient.DownloadString(url);

                        TwitchTV m_TwitchTV = JsonConvert.DeserializeObject<TwitchTV>(json);

                        this.BeginInvoke(m_OnShowTwitchTV, m_TwitchTV);
                        m_NeedKrakenUpdate = false;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error in LoadKraken: " + e.Message);
                }
                Thread.Sleep(3000);
            }
        }

        void StartTwitchAlerts()
        {
            if (Process.GetProcessesByName(Helpers.TWITCH_ALERTS_PROCESS_NAME).Length > 0)
            {
                m_TwitchAlertsProcess = Process.GetProcessesByName(Helpers.TWITCH_ALERTS_PROCESS_NAME)[0];
            }
            else
            {
                ProcessStartInfo twitchAlertsProcessInfo = new ProcessStartInfo(Properties.Settings.Default.TwitchAlerts);
                twitchAlertsProcessInfo.WindowStyle = ProcessWindowStyle.Normal;
                m_TwitchAlertsProcess = Process.Start(twitchAlertsProcessInfo);
            }
        }

        void SetFormTitle()
        {
            this.Text = string.Concat(string.IsNullOrWhiteSpace(m_TwitchAPIUser) ? "Twitch API" : m_TwitchAPIUser, " ", "Viewer", " v", GetVersion());
        }

        void TwitchForm_OnShowTwitchTV(TwitchTV twitchTV)
        {
            m_TwitchTV = twitchTV;
            if (twitchTV.stream != null)
            {
                if (m_LastViewers != twitchTV.stream.viewers)
                {
                    m_Blinks[lblViewers].loss = m_LastViewers > twitchTV.stream.viewers;
                    if (m_LastViewers < twitchTV.stream.viewers)
                    {
                        m_Visits++;
                        if (m_Visits > Properties.Settings.Default.MostVisits)
                        {
                            Properties.Settings.Default.MostVisits = m_Visits;
                            Properties.Settings.Default.Save();
                        }
                    }
                    if (Properties.Settings.Default.MostViewers < twitchTV.stream.viewers)
                    {
                        Properties.Settings.Default.MostViewers = twitchTV.stream.viewers;
                        Properties.Settings.Default.Save();
                    }
                    m_Blinks[lblViewers].count += BLINK_INC_TIME;
                    m_LastViewers = twitchTV.stream.viewers;
                }

                if (m_LastFollowers != twitchTV.stream.channel.followers)
                {
                    m_Blinks[lblFollowers].loss = m_LastFollowers > twitchTV.stream.channel.followers;
                    m_Blinks[lblFollowers].count += BLINK_INC_TIME;
                    m_Blinks[lblRecentFollower].count += BLINK_INC_TIME;
                    m_LastFollowers = twitchTV.stream.channel.followers;
                }

                if (m_LastViews != twitchTV.stream.channel.views)
                {
                    m_Blinks[lblViews].loss = m_LastViews > twitchTV.stream.channel.views;
                    m_Blinks[lblViews].count += BLINK_INC_TIME;
                    m_LastViews = twitchTV.stream.channel.views;
                }

                lblViewers.Text = string.Format("Viewers: {0:000}/{1:000}", twitchTV.stream.viewers, m_Visits);
                lblFollowers.Text = string.Format("Followers: {0:000}", twitchTV.stream.channel.followers);
                lblViews.Text = string.Format("Views: {0:000}", twitchTV.stream.channel.views);
                if (!m_UpTimer.IsRunning)
                {
                    m_UpTimer.Start();
                }
            }
            lblMostViewers.Text = string.Format("Viewers\n{0:0000}", Properties.Settings.Default.MostViewers);
            lblMostVisits.Text = string.Format("Visits\n{0:0000}", Properties.Settings.Default.MostVisits);

            CheckStreamStatus();
            m_NeedKrakenUpdate = true;
        }

        void CheckStreamStatus()
        {
            if (m_TwitchTV != null && m_TwitchTV.stream != null)
            {
                if (picOnline.BackColor != Color.Green)
                {
                    picOnline.BackColor = Color.Green;
                }
            }
            else
            {
                if (picOnline.BackColor != Color.Red)
                {
                    picOnline.BackColor = Color.Red;
                }
            }
        }

        Color GetColor(Control control)
        {
            return m_Blinks[control].loss ? Color.Red : Color.Green;
        }

        void timer1_Tick(object sender, EventArgs e)
        {
            if (!m_NeedKrakenUpdate)
            {
                m_NeedKrakenUpdate = true;
            }
            bool counting = false;
            foreach (var blinkKey in m_Blinks.Keys)
            {
                if (m_Blinks[blinkKey].count > 0)
                {
                    m_Blinks[blinkKey].count--;
                    blinkKey.ForeColor = blinkKey.ForeColor == GetColor(blinkKey) ? Color.Black : GetColor(blinkKey);
                    blinkKey.BackColor = blinkKey.BackColor == Color.Yellow ? GetColor(blinkKey) : Color.Yellow;
                    counting = true;
                }
                else
                {
                    blinkKey.ForeColor = m_Blinks[blinkKey].foreColor;
                    blinkKey.BackColor = m_Blinks[blinkKey].backColor;
                }
            }

            if (counting)
            {
                bool noLoss = m_Blinks.ToList().TrueForAll(x => x.Value.loss == false);
                if (noLoss)
                {
                    picOnline.BackColor = picOnline.BackColor == Color.Gold ? Color.Cyan : Color.Gold;
                }
                else
                {
                    picOnline.BackColor = picOnline.BackColor == Color.DarkRed ? Color.IndianRed : Color.DarkRed;
                }
                if (m_BlinkAll)
                {
                    this.Opacity = 1;
                }
            }
            else
            {
                CheckStreamStatus();
                if (m_BlinkAll)
                {
                    this.Opacity = tbOpacity.Value * 0.01;
                }
                m_BlinkAll = false;
            }

            if (!string.IsNullOrWhiteSpace(wbChat.DocumentText.Replace(@"\", "")))
            {
                HtmlAgilityPack.HtmlDocument htmlDoc = new HtmlAgilityPack.HtmlDocument();
                htmlDoc.Load(wbChat.DocumentStream);

                var lastMessages = htmlDoc.DocumentNode.SelectNodes("//span[@class='message' or @class='from']");
                if (lastMessages != null)
                {
                    var chatLine = lastMessages.Reverse().Take(2).Select(x => x.InnerText.Split('\r', '\n', ' ').ToList()).ToArray();
                    foreach (var item in chatLine)
                    {
                        item.RemoveAll(x => string.IsNullOrWhiteSpace(x));
                    }
                    var chatFrom = string.Join(" ", chatLine[1]);
                    var chatMessage = string.Join(" ", chatLine[0]);
                    var tempChatLine = string.Concat(chatFrom, ": ", chatMessage);
                    if (m_LastChatLine != tempChatLine && !m_IgnoreChatLines.Contains(tempChatLine))
                    {
                        m_Blinks[wbChat].count += BLINK_INC_TIME;
                        m_Blinks[btnToggleSize].count += BLINK_INC_TIME;
                        m_LastChatLine = tempChatLine;
                        if (Properties.Settings.Default.OpenOnChat)
                        {
                            ToggleMaximize();
                        }

                        if (chatMessage == "!blinkall")
                        {
                            m_BlinkAll = true;

                            foreach (var item in m_Blinks)
                            {
                                item.Value.count += BLINK_INC_TIME;
                            }
                        }
                    }
                }
            }
            
            if (m_FormSettings.IsValid(frmTwitchAlertsSettings.InputId.RecentFollower))
            {
                var lastFollower = File.ReadAllText(Properties.Settings.Default.RecentFollower);
                if (m_LastFollower != lastFollower)
                {
                    m_LastFollower = lastFollower;
                    m_Blinks[picRecentFollower].count += BLINK_INC_TIME;
                }
                lblRecentFollower.Text = lastFollower.Replace("Recent follower, ", "").Replace(", Thank You!", "");
            }

            if (m_Blinks[picRecentFollower].count <= 0)
            {
                if (m_TwitchTV != null)
                {
                    picRecentFollower.BackColor = m_TwitchTV.stream != null && Process.GetProcessesByName(Helpers.TWITCH_ALERTS_PROCESS_NAME).Length > 0 ? Color.Green : Color.Yellow;
                }
                else
                {
                    picRecentFollower.BackColor = Color.Red;
                }
            }
            lblUpTime.Text = string.Format("{0}", m_UpTimer.Elapsed);
        }

        List<string> m_IgnoreChatLines = new List<string>()
        {
            "Jtv: Welcome to the chat room!",
            ": Chat was cleared by a moderator",
        };

        void tbOpacity_ValueChanged(object sender, EventArgs e)
        {
            this.Opacity = tbOpacity.Value * 0.01;
        }

        void btnToggleSize_Click(object sender, EventArgs e)
        {
            this.Size = this.Size == m_MaxSize ? m_MinSize : m_MaxSize;
        }

        void ToggleMinimize()
        {
            if (this.Size == m_MaxSize)
            {
                this.Size = m_MinSize;
            }
        }

        void ToggleMaximize()
        {
            if (this.Size == m_MinSize)
            {
                this.Size = m_MaxSize;
            }
        }

        void lblUpTime_DoubleClick(object sender, EventArgs e)
        {
            if (m_UpTimer.IsRunning)
            {
                m_UpTimer.Stop();
            }
            else
            {
                m_UpTimer.Start();
            }
        }

        void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_UpTimer.Reset();
        }

        void restartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_UpTimer.Restart();
        }

        void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenSettings();
        }

        void OpenSettings()
        {
            if (m_FormSettings.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
            {
                StopTwitchAlertsProcess();
                StartTwitchAlertsThread();
            }
        }

        void OnUserNameChanged(object sender, EventArgs e)
        {
            ToolStripComboBox tscmb = sender as ToolStripComboBox;
            ContextMenuStrip parent = tscmb.Owner as ContextMenuStrip;
            if (parent != null)
            {
                string titleKey = parent.SourceControl.Tag.ToString();
                KeyEventArgs keyArgs = e as KeyEventArgs;
                if (keyArgs != null)
                {
                    if (keyArgs.KeyCode == Keys.Enter)
                    {
                        Helpers.StartUpOnUserChanged(sender, m_UserNameChanged[titleKey]);
                    }
                }
                else
                {
                    Helpers.StartUpOnUserChanged(sender, m_UserNameChanged[titleKey]);
                }
            }
        }

        Version GetVersion()
        {
            if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed)
            {
                return System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion;
            }
            else
            {
                return Assembly.GetExecutingAssembly().GetName().Version;
            }
        }

        void tscmbChangeUsers_TextChanged(object sender, EventArgs e)
        {
            ToolStripComboBox tscmb = sender as ToolStripComboBox;
            ContextMenuStrip parent = tscmb.Owner as ContextMenuStrip;
            if (parent.SourceControl != null && parent.SourceControl.Tag.Equals(CONTEXT_MENU_TITLE_TWITCH_API_USERS))
            {
                m_TwitchAPIUser = tscmb.Text;
            }
        }

        void ctxChangeUser_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ContextMenuStrip ctx = sender as ContextMenuStrip;
            
            object tag = ctx.SourceControl.Tag;

            ctx.Items[0].Text = tag != null ? tag.ToString() : CONTEXT_MENU_TITLE_BAD_TAG;

            foreach (var item in ctx.Items)
            {
                var userListChild = item as ToolStripComboBox;
                if (userListChild != null && userListChild.Name.EndsWith("Users"))
                {
                    userListChild.Items.Clear();
                    foreach (var channelUser in Properties.Settings.Default.ChannelUsers)
                    {
                        userListChild.Items.Add(channelUser);
                    }
                    break;
                }
            }
        }

        void chkOpenOnChat_CheckedChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.OpenOnChat = chkOpenOnChat.Checked;
            Properties.Settings.Default.Save();
        }

        void chkStream_CheckedChanged(object sender, EventArgs e)
        {
            ShowStreamCheckedState();
            LoadWebStream();
        }

    }
}
